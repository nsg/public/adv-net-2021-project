> This is a template for your project README, you should replace its content with an actual description of your project, but make sure to include all required information outlined below. After these three compulsory sections, you are free to use the rest of your README file as you see most useful.
> 

## Group info

| Group name | XYZ |  |  |
| --- | --- | --- | --- |
| Member 1 | Full name | nethz | email |
| Member 2 | Full name | nethz | email |
| Member 3 | Full name | nethz | email |

## Overview

In this section, provide a brief overview of your solution using *at most* 1500 characters (roughly three paragraphs).

Describe the main techniques that you used, including a short motivation why you used them. Where appropriate, indicate which part of your code implements the technique.

Keep in mind that this overview should allow us (TAs) to understand your solution on a high-level, but does not replace well-documented code. After reading this overview, we should be able to find our way around the files in your repository, and independently of this overview, each piece of code you submit should be properly commented in itself.

## Individual Contributions

In this section, note down 1 or 2 sentences *per team member* outlining everyone's contribution to the project. We want to see that everybody contributed, but you don't need to get into fine details. For example, write who contributed to which feature of your solution, but do *not* write who implemented a particular function. 

### <Full name 1>

Contribution(s) of <Full name 1>

### <Full name 2>

Contribution(s) of <Full name 2>

### <Full name 3>

Contribution(s) of <Full name 3>