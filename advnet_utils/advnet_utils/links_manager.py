"""Schedule failure events."""

from typing import Dict, List, Tuple
from advnet_utils.network_API import AdvNetNetworkAPI
from advnet_utils.input_parsers import parse_link_failures
import networkx as nx


class InvalidFailure(Exception):
    """Invalid failure scenario exception"""
    pass


class LinksManager(object):
    """Failure manager."""
    max_attempts = 100
    workers = []

    def __init__(
            self, net: AdvNetNetworkAPI, failures_file: str,
            constrains: Dict[str, int],
            added_links: List[Tuple[str, str]]):

        # get networkx node topology
        self.net = net
        self.topo = net.g.convertTo(nx.Graph)
        self.switches = net.p4switches()
        self.topo = self.topo.subgraph(self.switches).copy()

        # waypoint parameters used for interface management
        self.waypoint_output = ""
        self.waypoint_switches = []
        self.switches_ids = {}

        # get failures
        self.failures = parse_link_failures(failures_file)
        self.failures_raw = open(failures_file, "r").read()

        # links we added, this can be used to indicate we want to fail ADDED2
        self.added_links = added_links

        # replace ADDED by real link
        self._transform_added_to_real_link()

        # store failure constrains
        self.constrains = constrains

        # check if it is a valid failure scenario
        self._check_if_valid_failure_scenario()

        # Plan the link events from the provided spec
        self.link_events = self._get_link_events()

    # Scheduler failures.
    # ===================

    def set_reference_time(self, reference_time):
        """Sets the simulation t=0 to some specific unix time"""
        self.reference_time = reference_time

    def _enable_scheduler(self):
        """Enables the scheduler in some node at the main namespace"""
        # take the first switch
        if self.switches:
            self.namespace = self.switches[0]
        else:
            raise Exception(
                "Scheduler Error: There is 0 switches in the main namespace")

        self.net.enableScheduler(self.namespace)

    def _get_link_interfaces(self, node1, node2):
        """Get the real interfaces connecting two nodes"""

        info = self.net.getLink(node1, node2)[0]
        # checks if node1 matches the returned node 1... since the links
        # in the underlying returned structure do not follow an order
        if node1 == info["node1"]:
            intf1 = info["intfName1"]
            intf2 = info["intfName2"]
        else:
            intf1 = info["intfName2"]
            intf2 = info["intfName1"]
        return intf1, intf2

    def _get_link_event_cmd(self, intf, event):
        """Get link event cmd"""
        _cmd = "sudo ip link set dev {} {}".format(intf, event)
        # print(_cmd)
        return _cmd

    # def set_waypoint_filters(self, snapshot_length=250):
    #    """Sets the tcpdump filter for the waypointing"""
#
    #    topo = load_topo("/tmp/topology.json")
    #    # for every switch we set a filter for all interfaces
    #    for switch in topo.get_p4switches().keys():
    #        # if there is no waypoint rule we do not even monitor this switch
    #        if switch not in self.waypoint_switches:
    #            continue
    #        # get id, we use this for the pcap filter == tos
    #        switch_id = topo.get_p4switch_id(switch)
    #        # get all the interfaces that connect to P4 switch
    #        # I believe we do not need to capture node interfaces
    #        interfaces = []
    #        for neighbor in topo.get_p4switches_connected_to(switch):
    #            interfaces.append(
    #                topo.get_intfs()[switch][neighbor]["intfName"])
#
    #        # for some reason the filter only works
    #        # in this direction: "ip[1]==0 or (mpls and ip[1]==0)"
    #        # add mpls filter recursively
    #        # max 8 hops
    #        max_mpls_labels = 10
    #        cmd = 'tcpdump -i {} -s {} --direction=in -w {} '
    #        filter = 'ip[1] == {}'
    #        for _ in range(max_mpls_labels):
    #            filter += ' or (mpls and ip[1]=={})'
#
    #        for interface in interfaces:
    #            switch_ids = [switch_id] * (max_mpls_labels + 1)
    #            out_name = self.waypoint_output + "/" + interface + ".pcap"
    #            _cmd = cmd.format(interface, snapshot_length,
    #                              out_name)
    #            _filter = filter.format(*switch_ids)
    #            final_cmd = _cmd.split() + [_filter]
    #            # print(_cmd)
    #            run_command(final_cmd, outputfile=None)

    def _get_tcpdump_cmd(
            self, intf, switch_id, capture_id, snapshot_length=250,
            max_mpls_labels=10):
        """Returns the tcpdump command for waypointing"""

        cmd = 'tcpdump -i {} -s {} --direction=in -w {} '
        filter = 'ip[1] == {}'
        for _ in range(max_mpls_labels):
            filter += ' or (mpls and ip[1]=={})'

        switch_ids = [switch_id] * (max_mpls_labels + 1)
        out_name = self.waypoint_output + "/" + \
            intf + "_{}.pcap".format(capture_id)
        _cmd = cmd.format(intf, snapshot_length,
                          out_name)
        _filter = filter.format(*switch_ids)
        final_cmd = _cmd + '"' + _filter + '"'
        #final_cmd = _cmd.split() + [_filter]
        return final_cmd

    def _schedule_link_events(self, link_events):
        """Schedules all the link events (up/down)"""

        # each capture will have an id
        intf_to_id = {}
        for event, (node1, node2), event_time in link_events:
            # get interface names
            intf1, intf2 = self._get_link_interfaces(node1, node2)
            # set start time in the future
            start_time = self.reference_time + event_time
            # get link cmds
            _cmd1 = self._get_link_event_cmd(intf1, event)
            _cmd2 = self._get_link_event_cmd(intf2, event)

            _tcpdump_cmd1 = None
            _tcpdump_cmd2 = None

            # if up, we check if we need to add tcpdump
            if event == "up":
                if node1 in self.waypoint_switches:
                    if intf1 not in intf_to_id:
                        intf_to_id[intf1] = 0
                    else:
                        intf_to_id[intf1] += 1
                    _tcpdump_cmd1 = self._get_tcpdump_cmd(
                        intf1, self.switches_ids[node1], intf_to_id[intf1])
                    # tcpdump cmd for intf1
                    # self.net.addTask(
                    #    self.namespace, _tcpdump_cmd1, start=start_time+0.01)
                if node2 in self.waypoint_switches:
                    if intf2 not in intf_to_id:
                        intf_to_id[intf2] = 0
                    else:
                        intf_to_id[intf2] += 1
                    _tcpdump_cmd2 = self._get_tcpdump_cmd(
                        intf2, self.switches_ids[node2], intf_to_id[intf2])
                    # tcpdump cmd for intf2
                    # self.net.addTask(
                    #    self.namespace, _tcpdump_cmd2, start=start_time+0.01)

            # add commands
            if not _tcpdump_cmd1:
                self.net.addTask(self.namespace, _cmd1, start=start_time)
            else:
                self.net.addTask(
                    self.namespace, [_cmd1, _tcpdump_cmd1],
                    start=start_time)

            if not _tcpdump_cmd2:
                self.net.addTask(self.namespace, _cmd2, start=start_time)
            else:
                self.net.addTask(
                    self.namespace, [_cmd2, _tcpdump_cmd2],
                    start=start_time)

    def set_waypoint_params(self, outputdir, switches, switches_ids):
        """Sets waypoint parameters since they are needed when brining links up"""
        self.waypoint_output = outputdir
        self.waypoint_switches = switches
        self.switches_ids = switches_ids

    def start(self, reference_time):
        """Starts and schedules the link events"""
        # Sets t=0 in the simulation
        self.set_reference_time(reference_time)
        # adds scheduler
        self._enable_scheduler()
        # Adds link events to task manager
        self._schedule_link_events(self.link_events)

    # Helpers to compute the failures from spec.
    # ==========================================

    def _transform_added_to_real_link(self):
        """Parses the failures and replaces ADDED by a real link if exists"""

        for i, (link, _, _) in enumerate(self.failures):
            if link[0].startswith("ADDED"):
                index = int(link[0].split("_")[-1]) - 1
                # if the index is invalid we also raise
                if index > 0 and index >= len(self.added_links):
                    raise InvalidFailure(
                        "Invalid Link Failure: Added link with index {} is invalid".format(index))
                real_link = self.added_links[index]
                self.failures[i][0] = real_link

    def _check_if_valid_failure_scenario(self):
        """Checks if the failure scenario holds constrains and keeps network connected"""

        self._assert_faliure_scenario()
        self._assert_network_connectivity()

    def _assert_faliure_scenario(self):
        """Do a basic check of the failure scenario"""

        # verify time budget
        total_fail_time = sum([x[2] for x in self.failures])
        if total_fail_time > self.constrains["time_budget"]:
            raise InvalidFailure(
                "Exceeded Failure Budget: your total fail time is {}. Your budget is {}\n{}".
                format(
                    total_fail_time, self.constrains
                    ["time_budget"],
                    self.failures_raw))

        # verify start times and max fail time.
        for failure in self.failures:
            start_time = failure[1]
            duration = failure[2]
            end_time = start_time + duration
            # check if too early or too long
            if start_time < self.constrains["min_start"]:
                raise InvalidFailure(
                    "Invalid Link Failure (Too early) {}\n{}".format(
                        failure, self.failures_raw))
            if end_time > self.constrains["max_time"]:
                raise InvalidFailure(
                    "Invalid Link Failure (Too long): {}\n{}".format(
                        failure, self.failures_raw))

    def _assert_network_connectivity(self):
        """Checks if the network remains connected with the set of failures"""

        _link_events = self._get_link_events()
        # sort by event time
        _link_events_sorted = sorted(_link_events, key=lambda x: x[2])
        # valid sequence checking connectivity
        self._is_valid_link_event_sequence(_link_events_sorted)

    def _get_link_events(self):
        """Get all up/down events for the provided spec."""

        # events to schedule
        events = []
        for link, fail_time, duration in self.failures:
            events.append(("down", link, fail_time))
            events.append(("up", link, fail_time+duration))
        return events

    def _is_valid_link_event_sequence(self, events):
        """Checks if the sequence of events is valid"""

        # we apply events in batches. It could be that if one link goes down and
        # another up at the same time the network remains connected. If you
        # check events one by one you might trigger an Exception.

        # check if the network remains connected at any time

        # aggregate events
        _events = {}
        for action, edge, event_time in events:
            if event_time not in _events:
                _events[event_time] = [(action, edge)]
            else:
                _events[event_time].append((action, edge))

        # sort events by time
        _events = sorted(_events.items(), key=lambda x: x[0])

        for event_time, sub_events in _events:
            for action, edge in sub_events:
                if action == "down":
                    self.topo.remove_edge(*edge)
                elif action == "up":
                    self.topo.add_edge(*edge)

            # check if the graph is still connected
            if not nx.algorithms.components.is_connected(self.topo):
                raise InvalidFailure(
                    "Invalid Link Failure: Your failures disconnect the network!!!")
