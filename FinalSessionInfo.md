# Final session - Student info

---

Here are the details on the schedule of the AdvNet 2021 final session, including your **group's presentation slot** (see below).

<!-- TOC -->

- [Zoom session](#zoom-session)
- [Poster presentations](#poster-presentations)
- [Final plenary](#final-plenary)

<!-- /TOC -->

## Zoom session

The session will take place on Zoom. A specific room has been created for this session, which you can access using this link:

[Zoom link](https://ethz.zoom.us/j/66476223314?pwd=Si9jQ2IwUnZTZTBlZS9hM0hsdXFPUT09): https://ethz.zoom.us/j/66476223314?pwd=Si9jQ2IwUnZTZTBlZS9hM0hsdXFPUT09)  
Passcode: 202479  
Meeting ID: 664 7622 3314

> 💡 **Important!**  
You **must** join the Zoom meeting using either the **desktop or mobile app (version 5.3.0 or higher)**. Otherwise, you will not be able to self-select a breakout room.


The poster presentations will take place in break-out rooms. The rooms have been created in advance, one per group. Once you connect to the main room, you can self-select the break-out room you want to visit.

> 💡 I will let the meeting and break-out rooms open during the weekdays (office hour-ish). Try to connect and check that you can indeed independently go in the break-out rooms. If not, you probably need to update your Zoom client. **Do so before the session**!> 

If you connect but don't see me (Romain Jacob) in the meeting, break-out rooms won't be open. You can try to ping me on Teams, or simply try again later.

## Poster presentations

> December 21, 2021 **3:00 PM-5:00 PM**

Each group is assigned a 15 minutes time slot to present their poster to the TA team: 10 minutes presentation maximum, then questions from our side. The more time we have to ask questions, the better for you; so keep your presentation short!

You are invited to listen to the presentations from other groups, as long as it does not collide with your own presentation slot. If you want, just join the break-out room of the group you are interested in. 

> 🏆 The TA team will select one poster for the *AdvNet 2021 Best Poster Award*.

| Group | Time slot |
|:---|:---|
|Allen	|December 21, 2021 3:00 PM|
|Boole	|December 21, 2021 3:00 PM|
|Cerf	|December 21, 2021 3:15 PM|
|Dijkstra|	December 21, 2021 3:15 PM|
|Hamilton|	December 21, 2021 3:30 PM|
|Hopper	|December 21, 2021 3:30 PM|
|Kahn	|December 21, 2021 3:45 PM|
|Wirth	|December 21, 2021 3:45 PM|
|Lamport|	December 21, 2021 4:15 PM|
|Liskov	|December 21, 2021 4:15 PM|
|Rexford|	December 21, 2021 4:30 PM|
|Shannon|	December 21, 2021 4:30 PM|
|Turing	|December 21, 2021 4:45 PM|
|Knuth	|December 21, 2021 4:45 PM|

## Final plenary

> December 21, 2021 **5:15 PM-6:00 PM**

You are **all expected to attend** (please let us know in advance if you have a conflict).

We will announce the results of the competition and award the winning teams. Each winning team will have **5 minutes to present** their solution to the entire class. **No slides allowed!** You can only use your poster if you want to. We will then announce the 🏆*AdvNet 2021 Best Poster Award*. Finally, Laurent will conclude the session with some comments and remarks about this year's edition of AdvNet.

Hopefully, we will have some time left for a live feedback session: we would love to hear what you like/dislike about the course, as well as your suggestions for future editions!
